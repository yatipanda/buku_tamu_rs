<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Pengisian Buku Tamu | RSUD syamrabu</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="icon" href="../favicon.ico" type="image/x-icon" />

        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">
        
        <link rel="stylesheet" href="../admin/plugins/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="../admin/plugins/fontawesome-free/css/all.min.css">
        <link rel="stylesheet" href="../admin/plugins/ionicons/dist/css/ionicons.min.css">
        <link rel="stylesheet" href="../admin/plugins/icon-kit/dist/css/iconkit.min.css">
        <link rel="stylesheet" href="../admin/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
        <link rel="stylesheet" href="../admin/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
        <link rel="stylesheet" href="../admin/plugins/jquery-minicolors/jquery.minicolors.css">
        <link rel="stylesheet" href="../admin/plugins/datedropper/datedropper.min.css">
        <link rel="stylesheet" href="../admin/dist/css/theme.min.css">
        <script src="../admin/src/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="wrapper">
            <header class="header-top" header-theme="light">
                <div class="container-fluid">
                    <div class="d-flex justify-content-between">
                        <div class="top-menu d-flex align-items-center">
                            <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>
                            <div class="header-search">
                                <div class="input-group">
                                    <span class="input-group-addon search-close"><i class="ik ik-x"></i></span>
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon search-btn"><i class="ik ik-search"></i></span>
                                </div>
                            </div>
                            <button type="button" id="navbar-fullscreen" class="nav-link"><i class="ik ik-maximize"></i></button>
                        </div>
                    </div>
                </div>
            </header>

            <div class="page-wrap">
                <div class="app-sidebar colored">
                    <div class="sidebar-header">
                        <a class="header-brand" href="index.html">
                            <!-- <div class="logo-img">
                               <img src="../src/img/brand-white.svg" class="header-brand-img" alt="lavalite"> 
                            </div> -->
                            <span class="text">RSUD SYAMRABU</span>
                        </a>
                        <button type="button" class="nav-toggle"><i data-toggle="expanded" class="ik ik-toggle-right toggle-icon"></i></button>
                        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
                    </div>
                    
                    <div class="sidebar-content">
                        <div class="nav-container">
                            <nav id="main-menu-navigation" class="navigation-main">
                                <div class="nav-lavel">Forms</div>
                                <div class="nav-item active">
                                    <a href="pengisian_tamu.php" class="menu-item">Pengisian Buku Tamu</a>
                                </div>
                                <div class="nav-lavel">Tables</div>
                                <div class="nav-item active">
                                    <a href="daftar_tamu.php"><i class="ik ik-inbox"></i><span>Data Tamu</span></a>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="main-content">
                    <div class="container-fluid">
                        <div class="page-header">
                            <div class="row align-items-end">
                                <div class="col-lg-8">
                                    <div class="page-header-title">
                                        <i class="ik ik-edit bg-blue"></i>
                                        <div class="d-inline">
                                            <h5>Pengisian Buku Tamu</h5>
                                            <span>RSUD SYARIFAH AMBAMI</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="../index.html"><i class="ik ik-home"></i></a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#">Forms</a></li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3>Isi Data Berikut Ini :</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="mb-20">
                                          <form  action="../admin/insert.php" method="POST">
                                            <div class="row">
                                                <label class="col-sm-4 col-lg-2 col-form-label">No Rekam Medik</label>
                                                <div class="col-sm-8 col-lg-10">
                                                    <div class="input-group input-group-primary">
                                                        <span class="input-group-prepend">
                                                            <label class="input-group-text">1</label>
                                                        </span>
                                                        <input type="text" name="no_rm" class="form-control" placeholder="Isi Jika Ada">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-4 col-lg-2 col-form-label">Nama</label>
                                                <div class="col-sm-8 col-lg-10">
                                                    <div class="input-group input-group-primary">
                                                        <span class="input-group-prepend" >
                                                            <label class="input-group-text">2</label>
                                                        </span>
                                                        <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-4 col-lg-2 col-form-label">Tanggal Lahir</label>
                                                <div class="col-sm-8 col-lg-10">
                                                    <div class="input-group input-group-primary">
                                                        <span class="input-group-prepend">
                                                            <label class="input-group-text">3</label>
                                                        </span>
                                                        <input type="text" name="tgl_lahir" class="form-control datetimepicker-input" id="datepicker"  data-toggle="datetimepicker" data-target="#datepicker">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-4 col-lg-2 col-form-label">jenis kelamin</label>
                                                <div class="col-sm-8 col-lg-10">
                                                    <div class="input-group input-group-primary">
                                                        <span class="input-group-prepend">
                                                            <label class="input-group-text">4</label>
                                                            <select  class="dropdown" name="gender">
                                                                <option disabled="disabled" selected="selected">Jenis Kelamin</option>
                                                                <option>Perempuan</option>
                                                                <option>Laki-laki</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-4 col-lg-2 col-form-label">Alamat</label>
                                                <div class="col-sm-8 col-lg-10">
                                                    <div class="input-group input-group-primary">
                                                        <span class="input-group-prepend">
                                                            <label class="input-group-text">5</label>
                                                        </span>
                                                        <input type="text" class="form-control"  name="alamat" placeholder="Alamat Lengkap">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-4 col-lg-2 col-form-label">No. Telp</label>
                                                <div class="col-sm-8 col-lg-10">
                                                    <div class="input-group input-group-primary">
                                                        <span class="input-group-prepend" ">
                                                            <label class="input-group-text">6</label>
                                                        </span>
                                                        <input type="text" class="form-control" name="phone" placeholder="No Yang bisa dihubungi">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-4 col-lg-2 col-form-label">Keterangan</label>
                                                <div class="col-sm-8 col-lg-10">
                                                    <div class="input-group input-group-primary">
                                                        <span class="input-group-prepend" ">
                                                            <label class="input-group-text">7</label>
                                                        </span>
                                                        <input type="text" class="form-control" name="keterangan" placeholder="tujuan">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-lg-2">
                                                <div class="col-sm-8 col-lg-10">
                                                    <button name="submit type="button" class="btn btn-secondary"><i class="ik ik-clipboard"></i>Submit</button>
                                                </div>
                                            </div>
                                        <!-- Basic group add-ons end -->
                                    
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        
                    </div>
                </div>
                <footer class="footer">
                    <div class="w-100 clearfix">
                        <span class="text-center text-sm-left d-md-inline-block">Copyright © 2019 RSUD SYARIFAH AMBAMI RATO EBU.</span>
                        <span class="float-none float-sm-right mt-1 mt-sm-0 text-center">Crafted with <i class="fa fa-heart text-danger"></i> by <a>Nurhayati Fitriani</a></span>
                    </div>
                </footer>
            </div>
        </div>
        
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="../admin/src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
        <script src="../admin/plugins/popper.js/dist/umd/popper.min.js"></script>
        <script src="../admin/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../admin/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
        <script src="../admin/plugins/moment/moment.js"></script>
        <script src="../admin/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js"></script>
        <script src="../admin/plugins/jquery-minicolors/jquery.minicolors.min.js"></script>
        <script src="../admin/plugins/datedropper/datedropper.min.js"></script>
        <script src="../admin/dist/js/theme.min.js"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
